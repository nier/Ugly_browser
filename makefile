#!/bin/bash

mkdir -p ~/.config/ugly_browser/bookmarks
sudo cp -R $(pwd)/ugly_browser /usr/lib

sudo cp $(pwd)/ugly_browser.desktop /usr/share/applications
sudo cp $(pwd)/ugly_browser.sh /usr/bin

bash /usr/lib/ugly_browser/create_bookmark.sh -n searx.be -a https://searx.be

