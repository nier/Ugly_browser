#!/bin/bash

while test $# -gt 0; do
  case "$1" in
    -h|--help)
      echo "util to make bookmark (executable link)"
      echo "\n-h --help 	show brief help"
      echo "\n-n --name 	name of the bookmark"
      echo "\n-a --addr		url address"
      exit 1
      ;;
    -n)
      shift
      if test $# -gt 0; then
        export name=$1
      else
        echo "hasnt given name"
	exit 1
      fi
      shift
      ;;
    -a)
      shift
      if test $# -gt 0; then
        export addr=$1
      else
        echo "hasnt given address"
	exit 1
      fi
      shift
      ;;
    *)
      shift
      continue
      ;;
  esac
done

mkdir -p /home/$USER/.config/ugly_browser/bookmarks
app_code="#!/bin/bash\n\necho $addr | python3 /usr/lib/ugly_browser/ugly_browser.py\n"
echo -e $app_code > ~/.config/ugly_browser/bookmarks/$name.sh
chmod 777 /home/$USER/.config/ugly_browser/bookmarks/$name.sh

desktop_code="[Desktop Entry]\nEnecoding=utf-8\nVersion=1.0\nType=Application\nTerminal=false\nExec=sh /home/$USER/.config/ugly_browser/bookmarks/$name.sh\nName=$name\nComment=A link $addr that opens with ugly_browser\nCategories=Network;WebBrowser;\nIcon=/usr/lib/ugly_browser/ugly.png"
echo -e $desktop_code > ~/.local/share/applications/$name.desktop
