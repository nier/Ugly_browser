from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtWebEngineWidgets import *
import sys


class CustomWebEnginePage(QWebEnginePage):
    def __init__(self, parent, handler):
        super().__init__(parent)
        self.handler = handler

    def acceptNavigationRequest(self, url,  _type, isMainFrame):
        if (_type == QWebEnginePage.NavigationTypeLinkClicked):
            self.handler.make_new_window(url.toString())
            return False
        print(url)
        return super().acceptNavigationRequest(url,  _type, isMainFrame)


class MainWindow(QMainWindow):

    def __init__(self, url: str, handler):
        super(MainWindow, self).__init__()

        self.handler = handler
        self.init_web_engine_view()
        self.init_navigation_bar()
        self.open_url(url)
        
        self.browser.urlChanged.connect(self.update_url)
        self.showMaximized()

    def init_web_engine_view(self):
        self.browser = QWebEngineView()
        self.browser.setPage(CustomWebEnginePage(self, self.handler))
        self.setCentralWidget(self.browser)

    def init_navigation_bar(self):
        self.navbar = QToolBar()
        self.navbar.adjustSize()
        self.addToolBar(self.navbar)

        back_btn = QAction('<', self)
        back_btn.triggered.connect(self.browser.back)
        self.navbar.addAction(back_btn)
        
        forward_btn = QAction('>', self)
        forward_btn.triggered.connect(self.browser.forward)
        self.navbar.addAction(forward_btn)

        reload_btn = QAction('r', self)
        reload_btn.triggered.connect(self.browser.reload)
        self.navbar.addAction(reload_btn)

        self.url_bar = QLineEdit()
        self.url_bar.returnPressed.connect(lambda: self.open_url(self.url_bar.text()))
        self.navbar.addWidget(self.url_bar)

        new_session_btn = QAction('n', self)
        new_session_btn.triggered.connect(self.make_new_session)
        self.navbar.addAction(new_session_btn)

        bookmark_btn = QAction('b', self)
        bookmark_btn.triggered.connect(self.make_bookmark)
        self.navbar.addAction(bookmark_btn)

    def make_new_session(self):
        print("new session made")
        self.handler.make_new_window("https://duckduckgo.org")

    def open_url(self, url: str):
        if "://" not in url:
            url = "https://" + url
            self.url_bar.setText(url)
        self.browser.setUrl(QUrl(url))

    def update_url(self, q):
        self.url_bar.setText(q.toString())

    def make_bookmark(self):
        d = QDialog()

        label = QLabel('here u can create an executable links:', d)
        label.resize(250, 32)
        label.move(0, 0)

        addr_label = QLabel('addr:', d)
        addr_label.resize(200, 32)
        addr_label.move(0, 30)

        addr_line_box = QLineEdit(self.url_bar.text(), d)
        addr_line_box.resize(200, 32)
        addr_line_box.move(50, 30)

        name_label = QLabel('name:', d)
        name_label.resize(200, 32)
        name_label.move(0, 66)

        name_line_box = QLineEdit('', d)
        name_line_box.resize(200, 32)
        name_line_box.move(50, 66)


        button1 = QPushButton("ok",d)
        button1.move(125, 100)
        button1.resize(125, 49)
        button1.clicked.connect(lambda: create_bookmark(name_line_box.text(), addr_line_box.text()))

        button2 = QPushButton("close",d)
        button2.move(0, 100)
        button2.resize(125, 49)
        button2.clicked.connect(d.close)

        d.resize(250, 150)
        d.setWindowTitle("Dialog")
        d.setWindowModality(Qt.ApplicationModal)
        d.exec_()


class Handler:

    def __init__(self):
        self.windows = list()
        self.app = QApplication(sys.argv)
        QApplication.setApplicationName("ugly browser")

    def create_bookmark(name: str, addr: str):
        from subprocess import run
        run(["bash", "/usr/lib/ugly_browser/create_bookmark.sh", "-n", f"{name}", "-a", f"{addr}"])


    def make_new_window(self, url: str):
        print("creeatiing...")
        self.windows.append(MainWindow(str(url), self))
        self.windows[-1].show()
        print(url)

    def exec(self):
        self.app.exec()


if __name__ == '__main__':
    browser = Handler()
    url = input()
    browser.make_new_window(url)
    browser.exec()

